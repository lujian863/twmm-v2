<?php
	include("conf/conf.php");
	include("inc/conn.php");
	include("inc/query.php");
	include('inc/tools.php');
	date_default_timezone_set ('Asia/Shanghai'); //设定时区。php5.1以后，默认系统时间是GMT区的标准时，所以用date()函数打印出来的时间会+8H
	$weekarray=array("日","一","二","三","四","五","六"); //数组，用来存放周几的汉字
?>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="" method="post">
    <div class="searchBar">
		<table class="searchContent">
			<tr>
				<td>请选择场地：</td>
                 <td>
					<select class="combox" name="room_id">
<?php
	$r_sql = "select * from tb_room order by room_id";
	$r_rs = mysql_query($r_sql,$conn);
	while($r_row = mysql_fetch_array($r_rs)){
?>
	<option value="<?php echo $r_row['room_id']; ?>" 
    <?php
    	if(isset($_POST['room_id']) && $_POST['room_id'] == $r_row['room_id']){
			echo "selected=\"selected\"";	
		}	
	?> ><?php echo $r_row['room_name']?></option>
<?php
}
?>	
                    </select>
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">查&nbsp;询</button></div></div></li>
			</ul>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<table class="table" width="100%" layoutH="90">
		<thead>
			<tr>
				<th width="100" align="center">系统编号</th>
				<th width="180" align="center">场地名称</th>
				<th width="100" align="center">日 &nbsp;&nbsp;期</th>
				<th width="100" align="center">星 &nbsp;&nbsp;期</th>
				<th width="150" align="center">时 &nbsp;&nbsp;间</th>
				<th align="center">场地状态</th>
			</tr>
		</thead>
		<tbody>
<?php
if(isset($_POST['room_id'])){
	$roomNow = getRoomById($_POST['room_id']);
	$room_name =  $roomNow['room_name'];
	$room_status = $roomNow['room_status'];
}else{
	$firstRoom = getFirstRoom();
	$room_name =  $firstRoom['room_name'];
	$room_status = $firstRoom['room_status'];
}
if($room_status == 0){
	//循环体
	$js = 0;
	for($i = 0; $i <= 13; $i++){
		for($j = 1; $j <= 3; $j++){
			$js++;
?>
	<tr target="sid_user" rel="<?php echo $js; ?>">
    	<td><?php echo $js; ?></td>
        <td><?php echo $room_name;?></td>
        <td>
        <?php
        	$str_date = date('Y-m-d',strtotime($i." day"));
			echo $str_date;
		?>
        </td>
        <td>
        <?php
        	$zhou = "星 期 ".$weekarray[((date("w")+$i)%7)];
			echo $zhou;
		?>
        </td>
        <td>
        <?php echo changeTime($j); ?>
        </td>
        <td>
        	<?php 
				$sql_chk = "select * from tb_borrow where b_date = '".$str_date."' and b_time = ".$j." and b_roomname ='".$room_name."'"; 
				$rs_chk = mysql_query($sql_chk);
				if($row_chk = mysql_fetch_array($rs_chk)){
					if($row_chk['b_pass'] == '审核中'){
						echo "已被 <b>".$row_chk['b_yh']."</b> 申请，等待审核中...&nbsp;&nbsp;&nbsp;";
						echo "<a href=\"info.php?id=".$row_chk['b_id']."\" target=\"dialog\" width=\"400\">查看详情</a>";
					}else if($row_chk['b_pass'] == '已预订'){
						echo "已被 <b>".$row_chk['b_yh']."</b> 申请，已通过管理员审核。&nbsp;&nbsp;&nbsp;";
						echo "<a href=\"info.php?id=".$row_chk['b_id']."\" target=\"dialog\" width=\"400\">查看详情</a>";
					}else if($row_chk['b_pass'] == '已锁定'){
						echo "已被管理员 <b>".$row_chk['b_yh']."</b> 锁定，不提供申请。";
					}
				}else{
			?>
            <a href="ding_confirm.php?rid=<?php echo $room_name; ?>&d=<?php echo $str_date; ?>&t=<?php echo $j; ?>" target="dialog" width="500" height="500" title="预订场地">可预订</a>
       		<?php
				}
			?>
        </td>
    </tr>
<?php
		}
	}
}else{
	echo "
<tr>
<td><b> $room_name </b>已关闭，停止申请服务。详情请咨询管理员。</td>
</tr>
	";
}
?>       
		</tbody>
	</table>
</div>