<?php
	include('conf/conf.php');
	include('inc/conn.php');
	include('inc/tools.php');
?>
<?php
	$sql = "select * from tb_borrow where b_pass = '已预订'";
	$query = mysql_query($sql);
	$totalCount = mysql_num_rows($query);
	$pageNumShown = 10;
	$pageNum = 1;
    $numPerPage = 20;
	$orderField = "b_id";
    if(isset($_POST['pageNum'])){
        $pageNum = $_POST['pageNum'];
    }
    if(isset($_POST['numPerPage'])){
        $numPerPage = $_POST['numPerPage'];
    }
	if(isset($_POST['orderField'])){
        $orderField = $_POST['orderField'];
    }
	$fup_page = ($pageNum - 1) * $numPerPage;
	$rs = mysql_query($sql." order by {$orderField} limit {$fup_page},{$numPerPage}");
?>
<form id="pagerForm" action="pass_list.php" method="post">
   	<input type="hidden" name="pageNum" value="1" /><!--【必须】value=1可以写死-->
  	<input type="hidden" name="numPerPage" value="20" /><!--【可选】每页显示多少条-->
  	<input type="hidden" name="orderField" value="<?php echo $orderField; ?>" /><!--【可选】查询排序-->
</form> 
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="icon" href="exc.php?id=1" target="dwzExport" targetType="navTab" title="确实要导出这些记录吗?"><span>导出EXCEL</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="76">
		<thead>
			<tr>
				<th width="80"  align="center">系统编号</th>
				<th width="120" align="center">会议室名</th>
				<th width="120" align="center">申请单位</th>
				<th width="80" align="center">申请人</th>
				<th width="100" align="center">申请日期</th>
				<th width="130" align="center">申请时间</th>
				<th width="110" align="center">申请人电话</th>
				<th align="center">活动名称</th>
                <th width="150" align="center">操作</th>
			</tr>
		</thead>
		<tbody>
            <tr target="sid_user" rel="<?php echo $row['b_id']; ?>">
<?php
	while($row = mysql_fetch_array($rs)){
?>
                <td><?php echo $row['b_id']; ?></td>
                <td><?php echo $row['b_roomname']?></td>
                <td><?php echo $row['b_department']?></td>
                <td><?php echo $row['b_user']?></td>
                <td><?php echo $row['b_date']?></td>
                <td><?php echo changeTime($row['b_time']); ?></td>
                <td><?php echo $row['b_tel']?></td>
                <td><?php echo $row['b_act']?></td>
                <td>
                	<a href="remark.php?id=<?php echo $row['b_content']?>" target="dialog" title="备注">查看备注</a>&nbsp;&nbsp;&nbsp;
                </td>
            </tr>
<?php
	}
?>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
				<option value="200">200</option>
			</select>
			<span>条/每页，共<?php echo $totalCount; ?>条</span>
		</div>
		
        <div class="pagination" targettype="navTab" totalcount="<?php echo $totalCount; ?>" numperpage="<?php echo $numPerPage; ?>" pagenumshown="<?php echo $pageNumShown; ?>" currentpage="<?php echo $pageNum; ?>"></div>
	</div>
</div>
