<?php
	include('chkadmin.php');
	include('conf/conf.php');
	include('inc/conn.php');
	include('inc/query.php');
	$row = getRoomById($_GET['id']);
?>
<div class="pageContent">
	<form method="post" action="room_edit_sub.php" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxDone)">
		<div class="pageFormContent" layoutH="58">
			<div class="unit" style="font-size:16px" align="center">
            	<b>编辑场地</b>
			</div>
			<div class="unit">
				<label>场地名称：</label>
				<input type="text" name="room_name" size="30" value="<?php echo $row['room_name']; ?>" maxlength="30" class="required" />
                <input type="hidden" name="room_id" value="<?php echo $row['room_id']; ?>" />
			</div>			
		</div>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>