<?php
include('chkuser.php');
header('Content-Type:text/html;Charset=utf-8;');
include('conf/conf.php');
include('inc/conn.php');

$b_room_name = $_POST["b_room"];
$b_date = $_POST["b_date"];
$b_time = $_POST["b_time"];
$b_department = $_POST['b_department'];
$b_user = $_POST['b_user'];
$b_tel = $_POST['b_tel'];
$b_mail = $_POST['b_mail'];
$b_act = $_POST['b_act'];
$b_pass = "审核中";
$b_content = $_POST['b_content'];
$b_yh = $_SESSION["username"];

if(($b_user == null) || ($b_mail == null) || ($b_tel == null) || ($b_act == null) || ($b_department == null)){
	die("
{
	\"statusCode\":\"200\",
	\"message\":\"您提交的信息不完整！\",
	\"navTabId\":\"ding\",
	\"rel\":\"\",
	\"callbackType\":\"\",
	\"forwardUrl\":\"\",
	\"confirmMsg\":\"\"
}
	");
}

if($_SESSION['userkind'] != 0){//如果不是管理员就检查一下是否修改了日期，如果违法就结束执行
	//检查时间是否是在14天以内
	date_default_timezone_set ('Asia/Shanghai'); //设定时区。
	$sta = strtotime(date('Y-m-d'));
	$end = strtotime($b_date);
	$cle = $end - $sta; //得出时间戳差值
	$days = ceil($cle/3600/24); //得出一共多少天
	if($days >= 14){
		die("
			{
				\"statusCode\":\"300\",
				\"message\":\"您提交的时间违法！\",
				\"navTabId\":\"ding\",
				\"rel\":\"\",
				\"callbackType\":\"\",
				\"forwardUrl\":\"\",
				\"confirmMsg\":\"\"
			}
		");
	}
}

// 检查该时间段是否已被预订
$yan_sql = "select * from tb_borrow where b_roomname = '".$b_room_name."' and b_date = '".$b_date."' and b_time = '".$b_time."'";
$yan_rs = mysql_query($yan_sql);
$yan_row = mysql_fetch_array($yan_rs);
if($yan_row == null){
	$add_sql = "insert into tb_borrow (b_roomname,b_date,b_time,b_department,b_user,b_tel,b_mail,b_act,b_pass,b_content,b_yh,b_subtime) values ('".$b_room_name."','".$b_date."','".$b_time."','".$b_department."','".$b_user."','".$b_tel."','".$b_mail."','".$b_act."','".$b_pass."','".$b_content."','".$b_yh."',now())";
	if(mysql_query($add_sql)){ 
	//预定成功!点击确定返回首页查看。
		echo "
			{
				\"statusCode\":\"200\",
				\"message\":\"预定成功！\",
				\"navTabId\":\"ding\",
				\"rel\":\"\",
				\"callbackType\":\"closeCurrent\",
				\"forwardUrl\":\"\",
				\"confirmMsg\":\"\"
			}
		";
	}else{
		echo "
			{
				\"statusCode\":\"300\",
				\"message\":\"数据在提交过程中出错！请重试。\",
				\"navTabId\":\"ding\",
				\"rel\":\"\",
				\"callbackType\":\"closeCurrent\",
				\"forwardUrl\":\"\",
				\"confirmMsg\":\"\"
			}
		";
	}
}
else{
	//对不起，申请失败！可能其他用户已抢先预定了。。。请您重新选择。
	echo "
			{
				\"statusCode\":\"300\",
				\"message\":\"对不起，申请失败！可能其他用户已抢先预定了......请您重新选择！\",
				\"navTabId\":\"ding\",
				\"rel\":\"\",
				\"callbackType\":\"closeCurrent\",
				\"forwardUrl\":\"\",
				\"confirmMsg\":\"\"
			}
		";
}
?>