<?php
	include('chkadmin.php');
	header('Content-Type:text/html;Charset=utf-8;');
	include('conf/conf.php');
	include('inc/conn.php');
	include('inc/tools.php');

	$b_id = $_GET['id'];

	$m_sql = "select * from tb_borrow where b_id = '".$b_id."'";
	$m_rs = mysql_query($m_sql);
	$m_row = mysql_fetch_array($m_rs);
	$b_mail = $m_row['b_mail'];
	$b_department = $m_row['b_department'];
	$b_user = $m_row['b_user'];
	$b_act = $m_row['b_act'];
	$b_time = changeTime($m_row['b_time']);
	
	// 生成初始邮件内容
	$mailc = "<h3>恭喜您!您申请的".$m_row['b_roomname']."<br />使用时间".$m_row['b_date']."&nbsp;&nbsp;".$b_time."<br />已经获得管理员批准。<br />感谢您使用本系统！<br /></h3>";
?>
<div class="pageContent">
	<form method="post" action="pass_do.php" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxDone);">
		<div class="pageFormContent" layoutH="56">
			<p style="font-size:16px" align="center">
				批准确认
			</p>
			<p align="center">
				<label>申请单位：</label>
				<label><?php echo $b_department;?></label>
			</p>
			<p align="center">
				<label>申请人：</label>
				<label><?php echo $b_user;?></label>			
          	</p>
			<p align="center">
				<label>活动名称：</label>
				<label><?php echo $b_act;?></label>			
				<input type="hidden" name="b_id" value="<?php echo $b_id; ?>" />
            </p>
            
			<p style="font-size:16px" align="center">
				发送邮件
			</p>
            <p align="center">
				<label>收件人：</label>
				<label><?php echo $b_mail;?>@njau.edu.cn</label>
			</p>
            <p align="center">
				<label>邮件内容：</label>
				<textarea class="editor" name="b_mail_c" rows="15" cols="70" maxlength="500" tools="simple"><?php echo $mailc; ?></textarea>
			</p>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">确定</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>