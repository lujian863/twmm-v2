<?php
	include("conf/conf.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>南京农业大学团委场地管理系统</title>
<link href="themes/css/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="login">
		<div id="login_header">
			<h1 class="login_logo">
				<a href="#"><img src="
                <?php 
					if(TEST_MODEL){
						echo "themes/default/images/login_logo_test.gif";
					}else{
						echo "themes/default/images/login_logo.gif";	
					}
				?>
                " /></a>
			</h1>
			<div class="login_headerContent">
				<div class="navList">
					<ul>
						<li><a href="#">加入收藏</a></li>
						<li><a href="http://lvdao.njau.edu.cn/sug/index.php?id=南京农业大学团委场地管理系统 Version 2.0" target="_blank">意见反馈</a></li>
						<li><a href="#" target="_blank">使用帮助</a></li>
					</ul>
				</div>
				<h2 class="login_title"><img src="themes/default/images/login_title.png" /></h2>
			</div>
		</div>
		<div id="login_content">
			<div class="loginForm">
				<form name="f1" method="post" action="login_sub.php">
					<p>
						<label>用户名：</label>
						<input type="text" name="u_name" size="20" class="login_input" />
					</p>
					<p>
						<label>密码：</label>
						<input type="password" name="u_pass" size="20" class="login_input" />
					</p>
					<p>
						<label>验证码：</label>
						<input class="code" type="text" size="5" />
						<span><img src="themes/default/images/header_bg.png" alt="" width="75" height="24" /></span>
					</p>
					<div class="login_bar">
						<input class="sub" type="submit" value=" " />
					</div>
				</form>
			</div>
			<div class="login_banner"><img src="themes/default/images/login_banner.jpg" /></div>
			<div class="login_main">
				<ul class="helpList">
					<li><a href="#">下载用户手册</a></li>
					<li><a href="#">没有账号怎么办？</a></li>
					<li><a href="#">忘记密码怎么办？</a></li>
					<li><a href="#">为什么登录失败？</a></li>
				</ul>
				<div class="login_inner">
					<p>南京农业大学团委场地管理系统 Version 2.0</p>
					<p>全新系统 &nbsp;&nbsp; 全新架构 &nbsp;&nbsp; 全新操作体验 &nbsp; ！</p>
					<p>技术支持 ：&nbsp; 绿岛网络开发协会 &nbsp; DWZ团队</p>
				</div>
			</div>
		</div>
		<div id="login_footer">
			Copyright &copy; 2012 &nbsp; 
            <a href="http://tuanwei.njau.edu.cn/" target="_blank">南京农业大学团委</a> &nbsp; 
            <a href="http://lvdao.njau.edu.cn/" target="_blank">绿岛网络开发协会</a> &nbsp; 
            All Rights Reserved.
		</div>
	</div>
</body>
</html>