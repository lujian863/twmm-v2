<?php
	include('chkadmin.php');
	include('conf/conf.php');
	include('inc/conn.php');
	include('inc/query.php');
	$sql = "select * from tb_room order by room_id";
	$rs = mysql_query($sql);
?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="room_add.php" target="dialog"><span>添加</span></a></li>
			<li class="line">line</li>
			<li><a class="icon" href="exc.php?id=3" target="dwzExport" targetType="navTab" title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="75">
		<thead>
			<tr>
				<th width="100" align="center">系统编号</th>
				<th width="200" align="center">场地名称</th>
                <th width="200" align="center">场地状态</th>
                <th align="center">操作</th>
			</tr>
		</thead>
		<tbody>
<?php
while($row = mysql_fetch_array($rs)){
?> 
   <tr  rel="<?php echo $row['room_id']; ?>">
		<td><?php echo $row['room_id']; ?></td>
        <td><?php echo $row['room_name']; ?></td>
        <td><?php 
				if($row['room_status'] == 0){
					echo "已开启，正在接受申请";
				}else{
					echo "已关闭，停止接受申请";
				}
			?>
        </td>
        <td>
        <?php
        	if(isRoomOpen($row['room_id'])){
		?>
        	<a href="room_close.php?id=<?php echo $row['room_id']; ?>" target="ajaxTodo" title="提示：确定要关闭<b> <?php echo $row['room_name']; ?> </b>吗?<br />关闭后该场地将不提供申请服务。<br />关闭后可以再次开启。">关闭场地</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php		
			}else{
		?>
        	<a href="room_open.php?id=<?php echo $row['room_id']; ?>" target="ajaxTodo" title="提示：确定要开启<b> <?php echo $row['room_name']; ?> </b>吗?<br />开启后该场地将提供申请服务。<br />开启后可以再次关闭。">开启场地</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php	
			}
		?>
        	
          	<a href="room_edit.php?id=<?php echo $row['room_id']; ?>" target="dialog">修改名称</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="room_del_sub.php?id=<?php echo $row['room_id']; ?>" target="ajaxTodo" title="确定要删除<b> <?php echo $row['room_name']; ?> </b>吗?">删除场地</a>&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
    </tr>
<?php
} 	
 ?> 	
		</tbody>
	</table>
    </div>
</div>

