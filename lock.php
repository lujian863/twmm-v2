<?php
	include('chkadmin.php');
	include('conf/conf.php');
	include('inc/conn.php');
?>
<div class="pageContent">
	<form method="post" action="lock_do.php" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxDone);">
		<div class="pageFormContent" layoutH="56">
			<p style="font-size:16px" align="center">
				新增场地锁定
			</p>
			<p align="center">
				<label>场地名称：</label>
				<select class="combox" name="b_room">
                
<?php
	$r_sql = "select * from tb_room order by room_id";
	$r_rs = mysql_query($r_sql,$conn);
	while($r_row = mysql_fetch_array($r_rs)){
?>
	<option value="<?php echo $r_row['room_name']?>"><?php echo $r_row['room_name']?></option>
<?php
}
?>	
				</select>
			</p>
			<p align="center">
				<label>锁定日期：</label>
				<input type="text" name="b_date" class="date" size="30" /><a class="inputDateButton" href="javascript:;">选择</a>
			</p>
            <p align="center">
				<label>锁定时间：</label>
				<select class="combox" name="b_time">
                	<option value="1">上午 08:30-11:30</option>
					<option value="2">下午 14:00-17:00</option>
					<option value="3">晚上 18:00-21:00</option>
                </select>
			</p>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交申请</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>