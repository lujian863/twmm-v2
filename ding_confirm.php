<?php
	include('chkuser.php');
	include('inc/tools.php');
	header('Content-Type:text/html;Charset=utf-8;');
?>
<div class="pageContent">
	<form method="post" action="ding_do.php" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxDone);">
		<div class="pageFormContent" layoutH="56">
			<p style="font-size:16px" align="center">
				预订场地信息确认
			</p>
			<p align="center">
				<label>场地名称：</label>
				<input name="b_room" type="text" size="30" value="<?php echo $_GET['rid']; ?>" readonly="readonly"/>
			</p>
			<p align="center">
				<label>预订日期 ：</label>
				<input type="text" name="b_date" class="date" size="30" value="<?php echo $_GET['d']; ?>" /><a class="inputDateButton" href="javascript:;">选择</a>
			</p>
			<p align="center">
				<label>预订时间 ：</label>
				<input name="b_time_show" type="text" size="30" value="<?php echo changeTime($_GET['t']); ?>" readonly="readonly"/>
				 <input type="hidden" name="b_time" value="<?php echo $_GET['t']; ?>" />
            </p>
            
			<p style="font-size:16px" align="center">
				填写申请信息
			</p>
			<p align="center">
				<label>使用单位：</label>
				<input type="text" name="b_department" size="30" class="required" />
			</p>
			<p align="center">
				<label>联系人：</label>
				<input type="text" name="b_user" size="30" class="required" />
			</p>
            <p align="center">
				<label>联系电话：</label>
				<input type="text" name="b_tel" size="30" class="required" />
			</p>
            <p align="center">
				<label>南农邮箱：</label>
				<input type="text" name="b_mail" class="required" />
                <span class="unit">@njau.edu.cn</span>
			</p>
            <p align="center">
				<label>活动名称：</label>
				<input type="text" name="b_act" size="30" class="required" />
			</p>
            <p align="center">
				<label>备注信息：</label>
				<textarea name="b_content" style="width:240px;height:60px" maxlength="500">最多500字。</textarea>
			</p>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交申请</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>