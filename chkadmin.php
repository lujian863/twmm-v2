<?php
//登录验证
session_start();
header('Content-Type:text/html;Charset=utf-8;');
if(!isset($_SESSION['username']) || !isset($_SESSION['userkind'])){
	die("<script>alert('请登录！');window.location='login.php';</script>");	
}else if($_SESSION['userkind'] != 0){
	die ("<script>alert('您没有权限！');history.back();</script>");
}
?>